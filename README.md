

## Building

Requirements:

- Nightly Rust
- The Rust standard library compiled for your architecture. I have an ARMv7 router without NEON, so I install the arm-unknown-linux-musleabihf target (the armv7 build should work once Rust disables NEON when building it).
- sassc (`sudo apt-get install sassc` for Linux Mint 19+/Ubuntu 16.10+ or https://askubuntu.com/questions/849057/how-to-install-libsass-on-ubuntu-16-04)
- uglify-es (a Node.js package (sorry))

To build:

- Add the feed
- Edit the OpenWRT Makefile to have the desired target triple
- Enable the package with `make menuconfig`
- Build with `make` or `make package/nightshade/compile`
