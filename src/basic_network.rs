/* Copyright 2018 Jordan Miner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use rouille::{Request, Response};
use uci;

use super::{UCI_CONTEXT, Error, error_500, MainView};

struct WirelessNetworkSettings {
    ssid: String,
}

#[derive(BartDisplay)]
#[template = "assets/basic_network.bart"]
struct BasicNetworkView {
    wireless_networks: Vec<WirelessNetworkSettings>,
}

pub fn basic_network(request: &Request) -> Response {
    match basic_network_inner(&request) {
        Ok(res) => res,
        Err(err) => error_500(err),
    }
}

pub fn basic_network_inner(request: &Request) -> Result<Response, Error> {
    // It does not make sense adding or removing wifi-device sections.
    // wifi-iface sections are wireless networks.
    let uci_context = UCI_CONTEXT.lock().unwrap();

    let mut wl_networks = vec![];
    let mut i = 0;
    loop {
        let ssid = uci_context.get("wireless", &format!("@wifi-iface[{}]", i), "ssid");
        if let Err(uci::Error::SectionOrOptionNotFound) = ssid {
            break;
        }
        let ssid = ssid?;
        let ssid = ssid.get_string().map_err(|_| "@wifi-iface[0] not string")?;
        wl_networks.push(WirelessNetworkSettings {
            ssid: ssid.to_string(),
        });
        i += 1;
    }

    let view = BasicNetworkView {
        wireless_networks: wl_networks,
    };
    let main_view = MainView {
        title: "Basic Network".to_owned(),
        contents: view,
    };
    Ok(Response::text(format!("{}", main_view)))
    //Response::text(format!("Hello, world! SSID: {:?}", ssid))
}
