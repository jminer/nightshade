/* Copyright 2018 Jordan Miner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use std::borrow::Cow;
use std::ffi::CStr;
use std::marker::PhantomData;
use std::mem;
use std::os::raw::c_char;
use std::ptr;

use smallvec::SmallVec;
use uci_sys::*;

#[derive(Clone,Debug)]
pub enum Error {
    PackageNotFound,
    SectionOrOptionNotFound,
    UnknownOptionType,
    Other,
}

// copied from clear-coat
pub fn str_to_c_vec<'a: 'b, 'b, A: ::smallvec::Array<Item=u8>>(s: &'a str, buf: &'b mut SmallVec<A>) -> *const c_char {
    // `CString` in the std library doesn't check if the &str already ends in a null terminator
    // It allocates and pushes a 0 unconditionally. However, I can add the null to string literals
    // and avoid many allocations.
    if s.as_bytes().last() == Some(&0) && !s.as_bytes()[..s.len() - 1].contains(&b'\0') {
        s.as_bytes().as_ptr() as *const c_char
    } else {
        buf.grow(s.len() + 1);
        buf.extend(s.as_bytes().iter().map(|c| if *c == b'\0' { b'?' } else { *c }));
        buf.push(0);
        (&buf[..]).as_ptr() as *const c_char
    }
}

macro_rules! container_of {
    ( $ptr:expr, $outer_ty:ty, $member:ident ) => {{
        let dummy: $outer_ty = ::std::mem::uninitialized();
        let offset = (&dummy.$member as *const _ as usize) - (&dummy as *const _ as usize);
        ($ptr as usize - offset) as *mut $outer_ty
    }}
}

pub struct Context {
    ctx: *mut uci_context,
}

unsafe impl Send for Context {}

impl Context {
    pub fn new() -> Self {
        unsafe {
            let ctx = uci_alloc_context();
            Context {
                ctx,
            }
        }
    }

    //pub fn get_package(&'a self, name: &str) -> Package<'a> {
    //}
    
    pub fn get(&self, package: &str, section: &str, option: &str) -> Result<Value<'static>, Error> {
        unsafe {
            let p = self.lookup_ptr(package, section, option)?;
            if (*p.last).type_ != UCI_TYPE_OPTION {
                return Err(Error::Other);
            }
            let option: *mut uci_option = container_of!(p.last, uci_option, e);
            let val = match (*option).type_ {
                UCI_TYPE_STRING => {
                    let cstr = CStr::from_ptr((*option).v.string);
                    Value::String(Cow::Owned(cstr.to_string_lossy().into_owned()))
                }
                UCI_TYPE_LIST => unimplemented!(),
                _ => return Err(Error::UnknownOptionType),
            };
            Ok(val)
        }
    }

    pub fn set(package: &str, section: &str, option: &str, value: &Value) {

    }

    fn lookup_ptr(&self, package: &str, section: &str, option: &str) -> Result<uci_ptr, Error> {
        unsafe {
            // same characters that uci_validate_name() allows, called by uci_parse_ptr() which sets
            // extended flag if uci_validate_name() returns false
            let extended = !section
                .as_bytes()
                .iter()
                .all(|c| c.is_ascii_alphanumeric() || *c == b'_');

            let mut package_buf = SmallVec::<[u8; 16]>::new();
            let c_package = str_to_c_vec(package, &mut package_buf);
            let mut section_buf = SmallVec::<[u8; 16]>::new();
            let c_section = str_to_c_vec(section, &mut section_buf);
            let mut option_buf = SmallVec::<[u8; 32]>::new();
            let c_option = str_to_c_vec(option, &mut option_buf);

            let mut p = uci_ptr {
                package: c_package,
                section: c_section,
                option: c_option,
                .. mem::zeroed()
            };
            if extended {
                p.flags |= UCI_LOOKUP_EXTENDED;
            }
            match uci_lookup_ptr(self.ctx, &mut p, ptr::null_mut(), extended) {
                UCI_OK => {},
                UCI_ERR_NOTFOUND => return Err(Error::PackageNotFound),
                _ => return Err(Error::Other),
            }
            if (p.flags & UCI_LOOKUP_COMPLETE) == 0 {
                return Err(Error::SectionOrOptionNotFound);
            }
            
            Ok(p)
        }
    }
}

pub struct Package<'a> {
    phantom: PhantomData<&'a Context>,
}

#[derive(Clone,Debug)]
pub enum Value<'a> {
    String(Cow<'a, str>),
    List(Cow<'a, [String]>),
}

impl<'a> Value<'a> {
    pub fn get_string(&self) -> Result<&Cow<'a, str>, ()> {
        match self {
            &Value::String(ref s) => Ok(s),
            &Value::List(_) => Err(()),
        }
    }

    pub fn get_list(&self) -> Result<&Cow<'a, [String]>, ()> {
        match self {
            &Value::String(_) => Err(()),
            &Value::List(ref l) => Ok(l),
        }
    }
}

impl<'a> From<&'a str> for Value<'a> {
    fn from(s: &'a str) -> Self {
        Value::String(Cow::Borrowed(s))
    }
}

impl<'a> From<&'a [String]> for Value<'a> {
    fn from(s: &'a [String]) -> Self {
        Value::List(Cow::Borrowed(s))
    }
}
