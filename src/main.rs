/* Copyright 2018 Jordan Miner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#![feature(alloc_system, global_allocator, allocator_api)]

extern crate alloc_system;

use alloc_system::System;

#[global_allocator]
static A: System = System;


extern crate bart;
#[macro_use]
extern crate bart_derive;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rouille;
extern crate smallvec;
extern crate uci_sys;

use std::fmt::{Debug, Display};
// use std::net::{Ipv4Addr, TcpListener};
use std::sync::Mutex;

use rouille::Response;

use ip_traffic_monitor::*;

mod basic_network;
mod ip_traffic;
mod ip_traffic_monitor;
mod uci;

lazy_static! {
    static ref UCI_CONTEXT: Mutex<uci::Context> = Mutex::new(uci::Context::new());
}

static ASSETS_DIR: &str = "/usr/share/nightshade/assets";

#[derive(Debug)]
pub struct Error(String);

impl From<uci::Error> for Error {
    fn from(other: uci::Error) -> Self {
        Error(format!("{:?}", other))
    }
}

impl<'a> From<&'a str> for Error {
    fn from(other: &'a str) -> Self {
        Error(other.to_owned())
    }
}

fn error_404() -> Response {
    Response::empty_404()
}

fn error_500<T: Debug>(error: T) -> Response {
    Response::text(format!("{:?}", error)).with_status_code(500)
}

#[derive(BartDisplay)]
#[template = "assets/main.bart"]
pub struct MainView<T: Display> {
    title: String,
    contents: T,
}

fn main() {
    // https://en.wikipedia.org/wiki/0.0.0.0

    //rouille_test::rouille_main();
    // rocket_test::rocket_main();
    // let listener = TcpListener::bind((Ipv4Addr::new(127, 0, 0, 1), 80));
    // for stream in listener.incoming() {
        
    // }

    //let uci_context = uci::Context::new();

    let monitor = IpTrafficMonitor::new();

    let port = 8000;
    println!("Listening on port {}", port);
    rouille::start_server(("0.0.0.0", port), move |request| {
        let res = router!(request,
        (GET) (/) => {
            Response::text("Hello, world!")
        },

        (GET) (/assets) => {
            if let Some(request) = request.remove_prefix("/assets") {
                rouille::match_assets(&request, ASSETS_DIR)
            } else {
                error_404()
            }
        },

        (GET) (/basic-network) => {
            basic_network::basic_network(request)
        },

        (POST) (/basic-network) => {
            Response::text(format!("Hello, world!"))
        },

        (GET) (/ip-traffic-real-time) => {
            ip_traffic::real_time(&request)
        },

        (GET) (/ip-traffic-real-time-data) => {
            ip_traffic::real_time(&request)
        },

        _ => {
            error_404()
        });
        res
    });
}

