/* Copyright 2018 Jordan Miner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

use std::collections::HashMap;
use std::fs::File;
use std::io::{self, BufReader};
use std::net::IpAddr;
use std::thread;

struct IpAddrBandwidth {
    lan_tx: u64,
    lan_rx: u64,
    wan_tx: u64,
    wan_rx: u64,
}

pub struct IpTrafficMonitor {

}

impl IpTrafficMonitor {
    pub fn new() -> Self {
        let monitor_thread = thread::Builder::new()
            .name("ip_traffic_monitoring".to_owned())
            .spawn(IpTrafficMonitor::monitor);
        IpTrafficMonitor {
        }
    }

    fn read_bandwidth() -> io::Result<HashMap<IpAddr, IpAddrBandwidth>> {
        let file = File::open("/proc/ip_bw")?;
        let reader = BufReader::new(file);
        let bandwidth = HashMap::new();
        Ok(bandwidth)
    }

    fn monitor() {
        // loop {
        // }
    }
}
