/* Copyright 2018 Jordan Miner
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

"use strict";

let data = [
    {
        "startTime": "2018-02-17T06:48:02.226Z",
        "endTime": "2018-02-17T06:48:04.226Z",
        "ipAddresses": {
            "192.168.1.10": {
                "wanTx": 33000,
                "wanRx": 2000
            },
            "192.168.1.20": {
                "wanTx": 31000,
                "wanRx": 2100
            },
            "192.168.1.30": {
                "wanTx": 35000,
                "wanRx": 2500
            }
        }
    },
    {
        "startTime": "2018-02-17T06:48:04.226Z",
        "endTime": "2018-02-17T06:48:06.226Z",
        "ipAddresses": {
            "192.168.1.10": {
                "wanTx": 23000,
                "wanRx": 2000
            },
            "192.168.1.20": {
                "wanTx": 31000,
                "wanRx": 2100
            },
            "192.168.1.30": {
                "wanTx": 35000,
                "wanRx": 2500
            }
        }
    },
    {
        "startTime": "2018-02-17T06:48:06.226Z",
        "endTime": "2018-02-17T06:48:08.226Z",
        "ipAddresses": {
            "192.168.1.10": {
                "wanTx": 26000,
                "wanRx": 2000
            },
            "192.168.1.20": {
                "wanTx": 31000,
                "wanRx": 2100
            },
            "192.168.1.30": {
                "wanTx": 35000,
                "wanRx": 2500
            },
            "192.168.1.31": {
                "wanTx": 35000,
                "wanRx": 2500
            },
            "192.168.1.32": {
                "wanTx": 35000,
                "wanRx": 2500
            },
            "192.168.1.33": {
                "wanTx": 35000,
                "wanRx": 2500
            }
        }
    },
    {
        "startTime": "2018-02-17T06:48:08.226Z",
        "endTime": "2018-02-17T06:48:10.226Z",
        "ipAddresses": {
            "192.168.1.10": {
                "wanTx": 33000,
                "wanRx": 2000
            },
            "192.168.1.20": {
                "wanTx": 31000,
                "wanRx": 2100
            },
            "192.168.1.30": {
                "wanTx": 15000,
                "wanRx": 2500
            }
        }
    }
];

const txColors = [
    "#a6cee3",
    "#1f78b4",
    "#b2df8a",
    "#33a02c"
];
const rxColors = [
    "#fb9a99",
    "#e31a1c",
    "#fdbf6f",
    "#ff7f00"
];

const svg = d3.select(".real-time-chart");

const margin = {top: 10, right: 10, bottom: 50, left: 50};

const leftAxisG = svg.append("g")
    .attr("transform", "translate(" + margin.left + ", 0)");
const bottomAxisG = svg.append("g");

let minutesVisible = 3;

const updateBars = (xScale, yScale, className, stack, colorScale, half) => {
    // One group per IP address
    let group = svg.selectAll(`g.${className}`)
        .data(stack, d => d.key);
    group.exit().remove();
    group = group.enter().append("g")
            .classed(className, true)
        .merge(group)
            .attr("fill", d => colorScale(d.key));

    const widthFromData = d =>
        (xScale(new Date(d.data.endTime)) - xScale(new Date(d.data.startTime))) / 2;
    let rect = group.selectAll("rect")
        .data(d => d);
    rect.exit().remove();
    rect = rect.enter().append("rect")
        .merge(rect)
            .attr("x", d =>
                xScale(new Date(d.data.startTime)) + half * widthFromData(d))
            .attr("width", d => widthFromData(d))
            .attr("y", d => yScale(d[1]))
            .attr("height", d => yScale(d[0]) - yScale(d[1]));

};

const update = () => {
    const ipAddressesSet = new Set();
    for(let timeRecord of data)
        for(let ipAddr of Object.keys(timeRecord.ipAddresses))
            ipAddressesSet.add(ipAddr);
    const ipAddresses = Array.from(ipAddressesSet.values()).sort();

    const txStack = d3.stack()
        .keys(ipAddresses)
        .value((d, key) => d.ipAddresses[key] ? d.ipAddresses[key].wanTx : 0)
        (data);
    const rxStack = d3.stack()
        .keys(ipAddresses)
        .value((d, key) => d.ipAddresses[key] ? d.ipAddresses[key].wanRx : 0)
        (data);

    const svgRect = svg.node().getBoundingClientRect();

    //const now = new Date();
    const now = new Date("2018-02-17T06:48:10.226Z"); // for testing
    const xScale = d3.scaleTime()
        .domain([d3.timeMinute.offset(now, -minutesVisible), now])
        .range([margin.left, svgRect.width - margin.right]);

    const yMax = Math.max(
        d3.max(txStack, ipAddrD => d3.max(ipAddrD, d => d[1])),
        d3.max(rxStack, ipAddrD => d3.max(ipAddrD, d => d[1])));
    const yScale = d3.scaleLinear()
        .domain([0, yMax])
        .range([svgRect.height - margin.bottom, margin.top]);

    const txColorScale = d3.scaleOrdinal(txColors);
    const rxColorScale = d3.scaleOrdinal(rxColors);

    updateBars(xScale, yScale, "tx", txStack, txColorScale, 0);
    updateBars(xScale, yScale, "rx", rxStack, rxColorScale, 1);

    leftAxisG
        .attr("transform", "translate(" + margin.left + ", 0)")
        .call(d3.axisLeft(yScale));
    bottomAxisG
        .attr("transform", "translate(0, " + yScale(0) + ")")
        .call(d3.axisBottom(xScale));
};

update();
